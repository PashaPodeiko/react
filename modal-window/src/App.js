import React, { Component } from "react"
import Button from "./components/Button/Button"
import Modal from "./components/Modal/Modal"

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showModal: false,
      modal1: false,
      modal2: false,
    }
  }

  btnHandler = () => {
    this.setState({ showModal: !this.state.showModal })
  }
  btnClickModal1 = () => {
    this.setState({ modal1: !this.state.modal1 })
  }
  btnClickModal2 = () => {
    this.setState({ modal2: !this.state.modal2 })
  }

  render() {
    const { modal1, modal2 } = this.state

    const modalFirst = {
      textHeader: "Do you want to delete this file?",
      btnText:
        "Once you delete this file, it won’t be possible to undo this action.\n" +
        "Are you sure you want to delete it?",
    }

    const modalSecond = {
      textHeader: "Вы хотите удалить этот файл?",
      btnText:
        "После того, как вы удалите этот файл, отменить это действие будет невозможно. " +
        "Вы уверены, что хотите удалить его?",
    }

    return (
      <>
        <div className="wrap">
          <Button onClick={this.btnClickModal1} nameBtn="Open first modal" backgroundColor="red" />
          <Button onClick={this.btnClickModal2} nameBtn="Open second modal" backgroundColor="green" />
        </div>
        <Modal
          space={this.btnClickModal1}
          isOpen={modal1}
          textHeader={modalFirst.textHeader}
          btnText={modalFirst.btnText}
          modalClose={this.btnClickModal1}
          styleModal={"modal__first"}
          styleBtnOk={"modal__btn"}
          styleBtnCancel={"modal__btn"}
        />

        <Modal
          space={this.btnClickModal2}
          isOpen={modal2}
          textHeader={modalSecond.textHeader}
          btnText={modalSecond.btnText}
          modalClose={this.btnClickModal2}
          styleModal={"modal__second"}
          styleBtnOk={"modal__btn2"}
          styleBtnCancel={"modal__btn2 cancel"}
        />
      </>
    )
  }
}
