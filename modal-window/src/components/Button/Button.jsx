import React, { Component } from "react"
import './Button.scss'


export default class Button extends Component {
    render() {
		const { id, onClick, nameBtn, backgroundColor} = this.props
		 return(
            <div className="wrapper-button">
                <button id={id} 
					 onClick={onClick} 
					 className="button__first"  
					 style={{backgroundColor: backgroundColor}}>
               	{nameBtn}
                </button>
            </div>
        )
    }
}