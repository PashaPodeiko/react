import React from "react"
import "./Modal.scss"

export default class Modal extends React.Component {
  render() {
const {isOpen, space, textHeader, btnText, modalClose, styleModal, styleBtnOk, styleBtnCancel} = this.props
	return (
		
            <div className={`container-modal ${isOpen ? "open" : "close"}`} onClick={space}>
                <div className={styleModal} onClick={e => e.stopPropagation()}>
                    <div className="modal__header">
                        <h1 className="modal__question">{textHeader}</h1>
                        <div className="close" onClick={modalClose}> </div>
                    </div>
                    <div className="modal__wrapper-button">
                        <p className="modal__btnText">{btnText}</p>
                        <div className="modal__wrapper-btn">
                            <button className={styleBtnOk}>Ok</button>
                            <button className={styleBtnCancel} onClick={modalClose}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        )    
  }
}
